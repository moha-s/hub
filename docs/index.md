---
template: home.html
title: Save time in your DevOps projects
description: Create powerful CI/CD pipelines in 3 clicks using R2Devops! Select the plug and play jobs you need from our list, then trigger your pipeline and enjoy.
---

